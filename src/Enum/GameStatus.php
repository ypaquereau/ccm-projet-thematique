<?php

namespace App\Enum;

enum GameStatus: int
{
    case PENDING = 1;
    case STARTED = 2;
    case ABORDED = 3;
    case FINISHED = 4;
}
