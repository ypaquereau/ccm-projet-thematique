<?php

namespace App\Enum;

enum PlayerType: int
{
    case FIRE = 1;
    case ICE = 2;
}
