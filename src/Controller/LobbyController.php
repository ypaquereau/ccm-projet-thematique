<?php

namespace App\Controller;

use App\Entity\Game;
use App\Entity\Player;
use App\Entity\User;
use App\Enum\PlayerType;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/lobby', name: 'lobby_')]
class LobbyController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/', name: 'index')]
    public function index(): Response
    {
        return $this->render('lobby/index.html.twig', [
            'controller_name' => 'LobbyController',
        ]);
    }

    #[Route('/create', name: 'create', methods: ["POST"])]
    public function createLobby(): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        $game = new Game($user);
        $this->entityManager->persist($game);
        $this->entityManager->flush();

        return new JsonResponse(
            ['code' => $game->getCode()],
            Response::HTTP_CREATED
        );
    }

    #[Route('/join', name: 'join', methods: ["POST"])]
    public function joinLobby(Request $request, HubInterface $hub): JsonResponse
    {
        try {
            /** @var string $gameCode */
            $gameCode = $request->request->get('gameCode');

            if (empty($gameCode)) {
                throw new Exception('Code de la partie manquant');
            }

            $game = $this->entityManager->getRepository(Game::class)->findByCode($gameCode);

            if (empty($game)) {
                throw new Exception('Partie non trouvée');
            }

            if ($game->getPlayers()->count() >= 2) {
                throw new Exception('Partie pleine');
            }

            /** @var User $user */
            $user = $this->getUser();

            if ($game->isAlreadyInGame($user)) {
                throw new Exception('Vous êtes déjà dans la partie');
            }

            $game->addPlayer(new Player($game, $user, PlayerType::ICE));
            $this->entityManager->persist($game);
            $this->entityManager->flush();

            /** @var string $json */
            $json = json_encode(['player2' => $user->getUserIdentifier()]);
            $update = new Update(
                "{$_ENV['MERCURE_PUBLIC_URL']}/games/{$game->getCode()}",
                $json
            );

            $hub->publish($update);

            return new JsonResponse(['success' => 'Partie rejointe'], Response::HTTP_CREATED);
        } catch (Exception $e) {
            return new JsonResponse($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }
}
