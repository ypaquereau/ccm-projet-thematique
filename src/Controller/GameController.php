<?php

namespace App\Controller;

use App\Entity\Game;
use App\Entity\Player;
use App\Entity\User;
use App\Enum\GameStatus;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Routing\Annotation\Route;

class GameController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly HubInterface $hub
    ) {
    }

    #[Route('/game/{gameId}', name: 'home', defaults: ['gameId' => null], methods: ['GET'])]
    public function index(string $gameId = null): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        /** @var Game|null $game */
        $game = $this->entityManager->getRepository(Game::class)->findOneBy(['code' => $gameId]);

        if ($game === null || $game->getStatus() === GameStatus::FINISHED || $game->getPlayers()->count() < 2 || !$game->isAlreadyInGame($user)) {
            return $this->redirectToRoute('lobby_index');
        }

        /** @var Player $player */
        $player = $this->entityManager->getRepository(Player::class)->findOneBy(['game' => $game, 'user' => $user]);

        return $this->render('home/index.html.twig', [
            'game' => $_ENV['MERCURE_PUBLIC_URL'] . '/game/' . $game->getCode(),
            'gameCode' => $game->getCode(),
            'playerType' => $player->getType()->name // @phpstan-ignore-line
        ]);
    }

    #[Route('/game/{gameId}', name: 'send_game_event', methods: ['POST'])]
    public function broadcastEvent(Request $request, string $gameId): JsonResponse
    {
        $keyCode = $request->get('keyCode');
        $playerType = $request->get('playerType');
        $keyType = $request->get('keyType');

        /** @var string $data */
        $data = json_encode(['playerType' => $playerType, 'keyCode' => $keyCode, 'keyType' => $keyType]);

        $this->hub->publish(new Update(
            $_ENV['MERCURE_PUBLIC_URL'] . '/game/' . $gameId,
            $data
        ));

        return new JsonResponse();
    }
}
