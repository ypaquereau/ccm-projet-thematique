<?php

namespace App\Entity;

use App\Enum\GameStatus;
use App\Enum\PlayerType;
use App\Repository\GameRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GameRepository::class)]
class Game
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeImmutable $createdAt;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $finishedAt;

    #[ORM\Column(type: 'integer', enumType: GameStatus::class)]
    private GameStatus $status;

    /** @var Collection<int, Player> */
    #[ORM\OneToMany(mappedBy: 'game', targetEntity: Player::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $players;

    #[ORM\Column(type: 'string')]
    private string $code;

    public function __construct(User $user)
    {
        $this->players = new ArrayCollection();
        $this->createdAt = new DateTimeImmutable();
        $this->status = GameStatus::PENDING;
        $this->code = strtoupper(uniqid());

        $this->addPlayer(new Player($this, $user, PlayerType::FIRE));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getFinishedAt(): ?DateTimeImmutable
    {
        return $this->finishedAt;
    }

    public function setFinishedAt(?DateTimeImmutable $finishedAt): self
    {
        $this->finishedAt = $finishedAt;

        return $this;
    }

    public function getStatus(): ?GameStatus
    {
        return $this->status;
    }

    public function setStatus(GameStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return Collection<int, Player>
     */
    public function getPlayers(): Collection
    {
        return $this->players;
    }

    public function addPlayer(Player $player): self
    {
        if (!$this->players->contains($player)) {
            $this->players[] = $player;
            $player->setGame($this);
        }

        return $this;
    }

    public function isAlreadyInGame(User $user): bool
    {
        foreach ($this->players as $player) {
            if ($player->getUser() === $user) {
                return true;
            }
        }

        return false;
    }
}
