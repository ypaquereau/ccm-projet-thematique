<?php

use App\DataFixtures\UserFixtures;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\Persistence\ObjectRepository;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Liip\TestFixturesBundle\Services\DatabaseTools\AbstractDatabaseTool;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\User\UserInterface;

class ApplicationAvailabilityTest extends WebTestCase
{
    protected AbstractDatabaseTool $databaseTool;
    protected KernelBrowser $client;


    protected function setUp(): void
    {
        parent::setUp();
        $this->client = $this->createClient();

        /**
         * @var DatabaseToolCollection $databaseToolCollection
         */
        $databaseToolCollection = static::getContainer()->get(DatabaseToolCollection::class); // @phpstan-ignore-line
        $this->databaseTool = $databaseToolCollection->get();
    }

    /**
     * @dataProvider urlNotAuthProvider
     */
    public function testPageNotAuthIsSuccessful(string $url): void
    {
        $this->client->request('GET', $url);

        $this->assertResponseIsSuccessful();
    }

    /**
     * @dataProvider urlAuthProvider
     */
    public function testPageNotAuthIsFailed(string $url): void
    {
        $this->client->request('GET', $url);

        $this->assertResponseStatusCodeSame(302);
    }

    /**
     * @dataProvider urlAuthProvider
     */
    public function testPageAuthIsSuccessful(string $url): void
    {
        $this->databaseTool->loadFixtures([
            UserFixtures::class
        ]);

        /** @var ObjectRepository<User> $userRepository */
        $userRepository = static::getContainer()->get(UserRepository::class);

        /** @var UserInterface $user */
        $user = $userRepository->findOneBy(['username' => 'username']);

        $this->client->loginUser($user);
        $this->client->followRedirects(true);
        $this->client->request('GET', $url);
        $this->assertResponseIsSuccessful();
    }

    /**
     * @return Generator<array<int, string>>
     */
    public function urlNotAuthProvider(): Generator
    {
        yield['/login'];
        yield['/register'];
    }

    /**
     * @return Generator<array<int, string>>
     */
    public function urlAuthProvider(): Generator
    {
        yield['/lobby'];
        yield['/logout'];
    }
}
