<?php

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\TraceableValidator;

class UserTest extends KernelTestCase
{
    private \Faker\Generator $faker;
    private TraceableValidator $validator;

    protected function setUp(): void
    {
        $this->faker = Faker\Factory::create();
        /** @var TraceableValidator|null $validator */
        $validator = static::getContainer()->get('validator');

        if ($validator !== null) {
            $this->validator = $validator;
        }
    }

    public function testValidEntity(): void
    {
        $user = $this->getEntity($this->faker->userName(), $this->faker->password());
        self::bootKernel();

        /** @var ConstraintViolationList $errors */
        $errors = $this->validator->validate($user);
        $this->assertCount(0, $errors);
    }

    public function testTooShortUsername(): void
    {
        $user = $this->getEntity('u', $this->faker->password());
        self::bootKernel();

        /** @var ConstraintViolationList $errors */
        $errors = $this->validator->validate($user);
        $this->assertCount(1, $errors);
        $this->assertEquals('This value is too short. It should have 2 characters or more.', $errors->get(0)->getMessage());
    }

    public function testTooShortPassword(): void
    {
        $user = $this->getEntity($this->faker->userName(), $this->faker->password(1, 4));
        self::bootKernel();

        /** @var ConstraintViolationList $errors */
        $errors = $this->validator->validate($user);
        $this->assertCount(1, $errors);
        $this->assertEquals('This value is too short. It should have 5 characters or more.', $errors->get(0)->getMessage());
    }

    public function testTooLongUsername(): void
    {
        $user = $this->getEntity(str_repeat('u', 51), $this->faker->password());
        self::bootKernel();

        /** @var ConstraintViolationList $errors */
        $errors = $this->validator->validate($user);
        $this->assertCount(1, $errors);
        $this->assertEquals('This value is too long. It should have 50 characters or less.', $errors->get(0)->getMessage());
    }

    private function getEntity(string $username, string $password): User
    {
        return (new User())
            ->setUsername($username)
            ->setPassword($password);
    }
}
