DOCKER_COMPOSE = docker-compose -f docker-compose.yml -f docker-compose.dev.yml
EXEC_SYMFONY = $(DOCKER_COMPOSE) exec php symfony console
EXEC_COMPOSER = $(DOCKER_COMPOSE) exec php composer
EXEC_NPM = $(DOCKER_COMPOSE) exec php npm

build:
	$(DOCKER_COMPOSE) build

db-migrations:
	$(EXEC_SYMFONY) d:m:m --no-interaction

composer-install:
	$(EXEC_COMPOSER) install

start:
	DOCKER_BUILDKIT=1  $(DOCKER_COMPOSE) up -d

start_build:
	DOCKER_BUILDKIT=1  $(DOCKER_COMPOSE) up -d --build

logs:
	$(DOCKER_COMPOSE) logs -f

down:
	$(DOCKER_COMPOSE) down

exec-php:
	$(DOCKER_COMPOSE) exec php bash

npm-install:
	$(EXEC_NPM) install

npm-watch:
	$(EXEC_NPM) run watch

npm-dev:
	$(EXEC_NPM) run dev

npm-build:
	$(EXEC_NPM) run build

cs-fixer:
	$(DOCKER_COMPOSE) exec php vendor/bin/ecs check src tests --fix

phpstan:
	$(DOCKER_COMPOSE) exec php vendor/bin/phpstan analyse --configuration=phpstan.neon --memory-limit=4G

phpunit:
	$(DOCKER_COMPOSE) exec php env APP_ENV=test vendor/bin/phpunit tests

fix-permissions:
	sudo chown -R "$$(id -u):$$(id -g)" $$(ls | awk '{if($$1 != "db_data"){ print $$1 }}')