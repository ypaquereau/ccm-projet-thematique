FROM php:8.1-fpm AS base
RUN apt update \
    && apt install -y zlib1g-dev g++ git libicu-dev zip libzip-dev zip \
    && docker-php-ext-install intl opcache pdo pdo_mysql \
    && pecl install apcu \
    && docker-php-ext-enable apcu \
    && docker-php-ext-configure zip \
    && docker-php-ext-install zip

WORKDIR /var/www/symfony
COPY docker/php/wait-for-it.sh /usr/local/bin/wait-for-it
RUN chmod +x /usr/local/bin/wait-for-it
CMD wait-for-it -t 360 db:3306 -- bin/console d:m:m --no-interaction; php-fpm

FROM base AS dev
COPY --from=composer:2.3.5 /usr/bin/composer /usr/bin/composer
RUN curl -sS https://get.symfony.com/cli/installer | bash
RUN mv /root/.symfony/bin/symfony /usr/local/bin/symfony
RUN git config --global user.email "yoann.paquereau@gmail.com" \
    && git config --global user.name "Yoann Paquereau"
RUN apt install -y nodejs npm \
    && pecl install pcov \
    && docker-php-ext-enable pcov


FROM node:lts-alpine AS assets
WORKDIR /app
COPY . .
RUN npm install && npm run build

FROM base AS prod
ENV APP_ENV prod
ENV APP_DEBUG 0
COPY . .
RUN --mount=from=composer:2.3.5,source=/usr/bin/composer,target=/usr/bin/composer \
    composer install --prefer-dist --optimize-autoloader --classmap-authoritative --no-interaction --no-ansi --no-dev && \
    bin/console cache:clear --no-warmup && \
    bin/console cache:warmup
COPY --from=assets /app/public/build public/build
