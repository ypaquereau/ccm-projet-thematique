export default class Player {
    constructor(game, type, x, y){
        this.game = game
        this.controller = null
        this.type = type
        this.x = x
        this.y = y
        this.position = {
            x: this.x,
            y: this.y
        }
        this.width = 66
        this.height = 56
        this.velocity = {
            x: 0,
            y: .5
        }
        this.grounded = false
        if(type == 'FIRE'){
            var imageElementIdle = document.createElement('img')
            imageElementIdle.src = '/img/Player/mageFeuIdleSprite.png'
            var imageElementRunRight = document.createElement('img')
            imageElementRunRight.src = '/img/Player/mageFeuRunRightSprite.png'
            var imageElementRunLeft = document.createElement('img')
            imageElementRunLeft.src = '/img/Player/mageFeuRunLeftSprite.png'
        } else if(type == "ICE"){
            var imageElementIdle = document.createElement('img')
            imageElementIdle.src = '/img/Player/mageIceIdleSprite.png'
            var imageElementRunRight = document.createElement('img')
            imageElementRunRight.src = '/img/Player/mageIceRunRightSprite.png'
            var imageElementRunLeft = document.createElement('img')
            imageElementRunLeft.src = '/img/Player/mageIceRunLeftSprite.png'
        }
        this.frames = 0
        this.sprites = {
            idle: {
                idle: imageElementIdle,
                cropWidth: 58
            },
            run: {
                right: imageElementRunRight,
                left: imageElementRunLeft,
                cropWidth: 54
            }
        }

        this.currentSprite = this.sprites.idle.idle
        this.currentCropWidth = this.sprites.idle.cropWidth
    }

    draw(){
        this.game.c.drawImage(
            
            this.currentSprite,
            // Here we crop the image to only get the character at each time
            this.currentCropWidth * this.frames,
            0,
            this.currentCropWidth,
            56,
            // Here we put the position and weight/height
            this.position.x,
            this.position.y,
            this.width,
            this.height)
    }

    update(){

        this.position.x += this.velocity.x
        this.position.y += this.velocity.y
        if(this.position.y + this.height + this.velocity.y <= this.game.canvas.height){
            this.velocity.y += this.game.gravity
        } else {
            this.velocity.y = 0
            this.controller.jumpCounter = 0
            this.grounded = true
        }
    }
}