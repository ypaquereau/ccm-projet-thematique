export default class MageFireScore{
    constructor(game){
        var scoreFire = 0
        var x = 450
        var y = 10
        var image = '/img/Player/mageFireHead.png'
        this.x = x
        this.y = y
        this.game = game
        this.scoreFire = scoreFire
        this.imageName = image
        var imageElement = document.createElement('img')
        imageElement.src = image
        this.image = imageElement
        this.width = 38
        this.height = 38
    }

    draw(){
        this.game.c.drawImage(this.image, this.x, this.y)
        this.game.c.font = "40px Arial"
        this.game.c.fillStyle = "#b11501"
        if(this.scoreFire < 10){
            this.game.c.fillText('0' + this.scoreFire, this.x + 45, this.y + 36)
        } else {
            this.game.c.fillText(this.scoreFire, this.x + 45, this.y + 36)
        }
    }

    update(){
        this.scoreFire += 1
    }
}