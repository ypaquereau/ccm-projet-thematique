export default class Lava{
    constructor({x, y, image, game}){
        this.position = {
            x,
            y
        }
        this.imageName = image
        var imageElement = document.createElement('img')
        imageElement.src = image
        this.image = imageElement
        this.width = 64
        this.height = 64
        this.game = game
    }

    draw(){
        this.game.c.drawImage(this.image, this.position.x, this.position.y)
    }
}