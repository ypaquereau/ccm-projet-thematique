export default class Background{
    constructor({x, y, image, game}){
        this.position = {
            x,
            y
        }
        var imageElement = document.createElement('img')
        imageElement.src = image
        this.image = imageElement
        this.width = 1200
        this.height = 600
        this.game = game
    }

    draw(){
        this.game.c.drawImage(this.image, this.position.x, this.position.y)
    }
}