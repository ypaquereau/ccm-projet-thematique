export default class Game{
    constructor(mercureHubUrl, gameCode, playerType){

        // Canvas
        var canvas = document.createElement('canvas')
        canvas.width = 1200
        canvas.height = 600
        document.querySelector('body').appendChild(canvas)
        this.canvas = document.querySelector('canvas')
        this.c = this.canvas.getContext('2d')

        // Game property

        this.gravity = .7
        this.playerType = playerType

        // Other
        this.mercureHubUrl = mercureHubUrl
        this.gameCode = gameCode

        this.players = null

        this.initMercure()
    }

    initMercure() {
        const eventSource = new EventSource(this.mercureHubUrl)

        eventSource.onmessage = (event) => {
            const data = JSON.parse(event.data)

            // if (data.playerType !== this.playerType) {
                this.players.forEach(playerInfo => {
                    if(playerInfo.player.type == data.playerType){
                        // console.log(JSON.parse(event.data))
                        var jsonData = JSON.parse(event.data)
                        playerInfo.controller.keyInput(parseInt(jsonData.keyCode), jsonData.keyType)
                        // console.log(jsonData)
                    }
                })
            // }
        }
    }

    sendEvent(keyCode, keyType) {
        const data = new FormData()
        data.append('keyCode', keyCode)
        data.append('playerType', playerType)
        data.append('keyType', keyType)

        fetch(`/game/${this.gameCode}`, {
            method: 'POST',
            body: data
        })
    }
}
