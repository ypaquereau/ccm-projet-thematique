export default class Projectile{
    constructor({x, y, game, direction, type}){
        this.position = {
            x,
            y
        }
        this.direction = direction
        this.type = type
        if(this.direction < 0){
            if(this.type == 'FIRE') var image = '/img/Player/mageFireAttackLeft.png'
            if(this.type == 'ICE') var image = '/img/Player/mageIceAttackLeft.png'
        } else {
            if(this.type == 'FIRE') var image = '/img/Player/mageFireAttackRight.png'
            if(this.type == 'ICE') var image = '/img/Player/mageIceAttackRight.png'
        }
        this.imageName = image
        var imageElement = document.createElement('img')
        imageElement.src = image
        this.image = imageElement
        this.width = 33
        this.height = 43
        this.game = game
        this.velocity = 10 * direction
        this.frames = 0
    }

    draw(){
        this.game.c.drawImage(
            
            this.image,
            // Here we crop the image to only get the character at each time
            33 * this.frames,
            0,
            33,
            43,
            // Here we put the position and weight/height
            this.position.x,
            this.position.y,
            this.width,
            this.height)
    }

    update(){
        this.position.x += this.velocity
    }
}