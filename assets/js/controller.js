export default class Controller{
    constructor(player, game){
        this.player = player
        this.jumpLimit = 2
        this.attackLimit = 60
        this.attackCounter = 0
        this.jumpCounter = 0
        this.player.controller = this
        this.game = game
        this.isJumping = false
        this.isAttacking = false

        this.keys = {
            right: {
                pressed: false
            },
            left: {
                pressed: false
            },
        }
    }


    initController(){

     
            addEventListener('keydown', ({ keyCode }) => {
                console.log(keyCode)
                switch(keyCode) {
                    case 16:
                        if(this.isAttacking == false){
                            this.game.sendEvent(16, 'keydown')
                        }
                        break;
                    case 32:
                        if(this.isJumping == false){
                            this.game.sendEvent(32, 'keydown')
                        }
                        break;
                    case 81:
                        if(this.keys.left.pressed == false){
                            this.game.sendEvent(81, 'keydown')
                        }
                        break;
                    case 68:
                        if(this.keys.right.pressed == false){
                            this.game.sendEvent(68, 'keydown')
                        }
                        break;
                }
            })
            
            addEventListener('keyup', ({ keyCode }) => {
                switch(keyCode) {
                    case 16:
                        this.game.sendEvent(16, 'keyup')
                        break;
                    case 32:
                        this.game.sendEvent(32, 'keyup')
                        break;
                    case 81:
                        this.game.sendEvent(81, 'keyup')
                        break;
                    case 68:
                        this.game.sendEvent(68, 'keyup')
                        break;
                }
 
            })
      
    } 


    keyInput(keyCode, keyType){

        if(keyType == "keydown"){
 
            switch(keyCode) {
                case 16:
                    if(this.isAttacking == false){
                        this.isAttacking = true
                    }
                    break;
                case 32:
                    if(this.isJumping == false){
                        this.isJumping = true
                    }
                    break;
                case 81:
                    if(this.keys.left.pressed == false){
                        this.player.frames = 0
                        this.keys.left.pressed = true
                    }
                    this.player.currentSprite = this.player.sprites.run.left
                    this.player.currentCropWidth = this.player.sprites.run.cropWidth
                    break;
                case 68:
                    if(this.keys.right.pressed == false){
                        this.player.frames = 0
                        this.keys.right.pressed = true
                    }
                    this.player.currentSprite = this.player.sprites.run.right
                    this.player.currentCropWidth = this.player.sprites.run.cropWidth
                    break;

            }

            
        } else if(keyType == "keyup"){
            switch(keyCode) {
                case 16:
                    if(this.isAttacking == true){
                        this.isAttacking = false
                    }
                    break;
                case 32:
                    if(this.isJumping == true){
                        if(this.jumpCounter <= this.jumpLimit){
                            this.player.velocity.y = 0
                        }
                        this.isJumping = false
                    }
                break;
                case 81:
                    if(this.keys.left.pressed == true){
                        this.player.frames = 0
                        this.keys.left.pressed = false
                    }
                    this.player.currentSprite = this.player.sprites.idle.idle
                    this.player.currentCropWidth = this.player.sprites.idle.cropWidth
                break;
                case 68:
                    if(this.keys.right.pressed == true){
                        this.player.frames = 0
                        this.keys.right.pressed = false
                    }
                    this.player.currentSprite = this.player.sprites.idle.idle
                    this.player.currentCropWidth = this.player.sprites.idle.cropWidth
                break;
    
            }
        }
        

    }
}