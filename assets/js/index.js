import Game from './game.js'
import Player from './player.js'
import Controller from './controller.js'
import Platform from './platform.js'
import Projectile from './projectile.js'
import Background from './background.js'
import MageFireScore from './mageFireScore.js'
import MageIceScore from './mageIceScore.js'

const fps = 60
const fpsSprite = 10
var now
var then = Date.now()
var thenSprite = Date.now()
var interval = 1000/fps
var intervalSprite = 1000/fpsSprite
var delta
var deltaSprite
var backgroundImage = '/img/background/background-image.png'
var offSet = 0
var offsetAttack = 0
var otherType = (playerType == 'FIRE') ? 'ICE' : 'FIRE'
var direction = 0
const game = new Game(mercureHubUrl, gameCode, playerType)
const mageFireScore = new MageFireScore(game)
const mageIceScore = new MageIceScore(game)
const players = []
var positionSelf = 0
var positionOther = 0
if(playerType == 'FIRE'){
    positionSelf = 50
    positionOther = 1100
} else if(playerType == 'ICE'){
    positionSelf = 1100
    positionOther = 50
}
const playerSelf = new Player(game, playerType, positionSelf, 400)
const playerOther = new Player(game, otherType, positionOther, 400)

const controllerSelf = new Controller(playerSelf, game)
const controllerOther = new Controller(playerOther, game)

players.push({player : playerSelf, controller: controllerSelf})
players.push({player : playerOther, controller: controllerOther})
game.players = players
const platforms = []
const projectiles = []
const background = new Background({
    x: 0,
    y: 0,
    image: backgroundImage,
    game
})
const lavas = []



// Platforms for ground

for(let i=0; i<25; i++){
    platforms.push(new Platform({
        x: i*53,
        y: 550,
        image: '/img/tiles_rock/tile_rock_2.png',
        game
    }))  
}

// Left platform in the air
var offset = 0;
for(let i=1; i<=3;i++){
    if(i == 1){
        offset = 5
    } else {
        offset = 0
    }

    platforms.push(new Platform({
        x: (i*53) + 150,
        y: 350,
        image: '/img/tiles_rock/tile_rock_' + i + '.png',
        game
    }))  
    platforms.push(new Platform({
        x: (offset+i*53) + 150,
        y: 400,
        image: '/img/tiles_rock/floating_diagonal_tile_' + i + '.png',
        game
    })) 

}

// Right platform in the air

var offset = 0;
for(let i=1; i<=3;i++){
    if(i == 1){
        offset = 5
    } else {
        offset = 0
    }

    platforms.push(new Platform({
        x: (i*53) + 783,
        y: 350,
        image: '/img/tiles_rock/tile_rock_' + i + '.png',
        game
    }))  
    platforms.push(new Platform({
        x: (offset+i*53) + 783,
        y: 400,
        image: '/img/tiles_rock/floating_diagonal_tile_' + i + '.png',
        game
    })) 

}

// Middle platform in the air

var offset = 0;
for(let i=1; i<=3;i++){
    if(i == 1){
        offset = 5
    } else {
        offset = 0
    }

    platforms.push(new Platform({
        x: (i*53) + 468,
        y: 150,
        image: '/img/tiles_rock/tile_rock_' + i + '.png',
        game
    }))  
    platforms.push(new Platform({
        x: (offset+i*53) + 468,
        y: 200,
        image: '/img/tiles_rock/floating_diagonal_tile_' + i + '.png',
        game
    })) 

}

controllerSelf.initController()

function animate(){
    requestAnimationFrame(animate)

    now = Date.now()
    delta = now - then
    deltaSprite = now - thenSprite

    if(delta > interval){

        game.c.clearRect(0, 0, game.canvas.width, game.canvas.height)
        background.draw()
        lavas.forEach((lava) => {
            lava.draw()
        })

       

        // TODO: Make a foreach on the player/controller and do rules for both the players

        players.forEach(playerInfo => {
            var player = playerInfo.player
            var controller = playerInfo.controller
            if(controller.attackCounter < 60) controller.attackCounter += 1
            if(controller.keys.right.pressed){
                player.velocity.x = 6
            } else if(controller.keys.left.pressed){
                player.velocity.x = -6
            } else player.velocity.x = 0

            if(controller.isJumping){
                controller.jumpCounter += 1
                if(controller.jumpCounter <= controller.jumpLimit || player.grounded){
                    player.grounded = false
                    player.velocity.y -= 9
                }
            }

            if(controller.isAttacking){
                if(controller.attackCounter == controller.attackLimit){
                    if(player.velocity.x < 0){
                        direction = -1
                        offsetAttack = -30
                    } else {
                        direction = 1
                        offsetAttack = 50
                    }
                    projectiles.push(new Projectile({
                        x: player.position.x + offsetAttack,
                        y: player.position.y,
                        game,
                        direction: direction,
                        type: player.type
                    }))  
                    controller.attackCounter = 0
                }
            }
        })


        platforms.forEach((platform) => {

            platform.draw()

            // console.log(platform.imageName)
            if(platform.imageName == '/img/tiles_rock/floating_rock_tile_2.png'){
                offSet = -2
            } else if(platform.imageName == '/img/tiles_rock/horizontal_tile.png'){
                offSet = 0
            } else if(platform.imageName == '/img/tiles_rock/single_tile_rock.png'){
                offSet = 5
            } else if(platform.imageName == '/img/tiles_rock/floating_rock_tile_1.png'){
                offSet = 2
            } else if(platform.imageName.startsWith('/img/tiles_bridge')){
                offSet = 20
            } else {
                offSet = 0
            }

            players.forEach(playerInfo => {

                var player = playerInfo.player
                var controller = playerInfo.controller

                if(player.position.y + player.height <= platform.position.y + offSet && player.position.y + player.height + player.velocity.y >= platform.position.y + offSet && player.position.x + player.width - 10 >= platform.position.x && player.position.x <= platform.position.x + platform.width){
                    player.velocity.y = 0
                    controller.jumpCounter = 0
                    player.grounded = true
                    player.isFalling = false
                }
    
                if(player.position.y >= platform.position.y + platform.height && player.position.y + player.velocity.y <= platform.position.y + platform.height && player.position.x + player.width - 10 >= platform.position.x && player.position.x <= platform.position.x + platform.width){
                    player.velocity.y = 0
                }
    
                if(player.position.x + player.width - 10 <= platform.position.x && player.position.x + player.width - 10 + player.velocity.x >= platform.position.x && player.position.y + player.height >= platform.position.y + offSet && player.position.y <= platform.position.y + platform.height + offSet){
                    player.velocity.x = 0
                }
    
                if(player.position.x >= platform.position.x + platform.width && player.position.x + player.velocity.x <= platform.position.x + platform.width && player.position.y + player.height >= platform.position.y + offSet && player.position.y <= platform.position.y + platform.height + offSet){
                    player.velocity.x = 0
                }
                if(player.position.x + player.velocity.x < 0){
                    player.velocity.x = 0
                }
                if(player.position.x + player.velocity.x > 1140){
                    player.velocity.x = 0
                }
            })

        })

        projectiles.forEach((projectile) => {
            projectile.draw()

            players.forEach(playerInfo => {
                var player = playerInfo.player

                // Position y du joueur = en haut de sa tete
                if(player.type != projectile.type){
                    if(projectile.position.y + projectile.height >= player.position.y && projectile.position.y + projectile.height <= player.position.y + player.height && projectile.position.x <= player.position.x + player.width && projectile.position.x + projectile.width >= player.position.x || projectile.position.y >= player.position.y && projectile.position.y <= player.position.y + player.height && projectile.position.x <= player.position.x + player.width && projectile.position.x + projectile.width >= player.position.x){
                        var i = projectiles.indexOf(projectile)
                        projectiles.splice(i, 1);
                        if(player.type == "FIRE") mageIceScore.update()
                        if(player.type == "ICE") mageFireScore.update()
                    }
                    
                }
            })
        })

        players.forEach(playerInfo => {

            var player = playerInfo.player
            player.update()
            player.draw()
        })

        projectiles.forEach(projectile => {
            projectile.update()
        })

        mageFireScore.draw()
        mageIceScore.draw()

        then = now - (delta % interval)
    }
    if(deltaSprite > intervalSprite){
        players.forEach(playerInfo => {

            var player = playerInfo.player
            player.frames++
            if(player.frames >= 8 && player.currentSprite === player.sprites.run.right){
                player.frames = 0
            } else if(player.frames >= 8 && player.currentSprite === player.sprites.run.left){
                player.frames = 0
            } else if(player.frames >= 14 && player.currentSprite === player.sprites.idle.idle){
                player.frames = 0
            }
        })

        projectiles.forEach(projectile => {
            var i = projectiles.indexOf(projectile)
            projectile.frames++
            if(projectile.frames >= 3){
                projectile.frames = 0
            }
            if(projectile.position.x > 1200 || projectile.position.x < 0){
                projectiles.splice(i, 1);
            }
            
        })


        thenSprite = now - (deltaSprite % intervalSprite)
    }

}

animate()