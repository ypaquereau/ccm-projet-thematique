export default class MageIceScore{
    constructor(game){
        var scoreIce = 0
        var x = 710
        var y = 10
        var image = '/img/Player/mageIceHead.png'
        this.x = x
        this.y = y
        this.game = game
        this.scoreIce = scoreIce
        this.imageName = image
        var imageElement = document.createElement('img')
        imageElement.src = image
        this.image = imageElement
        this.width = 38
        this.height = 38
    }

    draw(){
        this.game.c.drawImage(this.image, this.x, this.y)
        this.game.c.font = "40px Arial"
        this.game.c.fillStyle = "#2131ff"
        if(this.scoreIce < 10){
            this.game.c.fillText('0' + this.scoreIce, this.x - 50, this.y + 36)
        } else {
            this.game.c.fillText(this.scoreIce, this.x - 50, this.y + 36)
        }

    }

    update(){
        this.scoreIce += 1
    }
}