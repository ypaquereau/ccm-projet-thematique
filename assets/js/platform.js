export default class Platform{
    constructor({x, y, image, game}){
        this.position = {
            x,
            y
        }
        this.imageName = image
        var imageElement = document.createElement('img')
        imageElement.src = image
        this.image = imageElement
        this.width = 50
        this.height = 50
        this.game = game
    }

    draw(){
        this.game.c.drawImage(this.image, this.position.x, this.position.y)
    }
}