# What you need

* Docker
* Docker-compose
* Make command

# Installation

* Run `make start_build`
* Run `make composer-install`
* Run `make db-migrations`
* Run `make npm-install`
* Run `make npm-dev`

# Tips

* Run `make npm-watch` to rebuild assets at every changes
* Run `make npm-dev` when you add a new file in `webpack.config.js`
